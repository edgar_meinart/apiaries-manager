Rails.application.routes.draw do

  root to: 'home#index'
  devise_for :users

  resources :jobs, only: [ :index, :update, :edit, :destroy ]

  resources :apiaries do
    resources :families do 
      resource :cards, only: [:new, :create, :show ]
      resources :jobs
      resources :journals
      get 'dublicate', to: :dublicate, on: :member
    end
  end
  resources :users, only: [ :show ]
end
