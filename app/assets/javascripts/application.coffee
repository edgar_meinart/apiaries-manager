#= require jquery
#= require jquery_ujs
#= require jquery.ui.all
#= require magnific_popup
#= require content_menu
#= require spectrum_colorpicker
#= require datepicker
#= require twitter/bootstrap
#= require_tree .


$ ->
  $('a.disabled').on 'click', (event) ->
    event.preventDefault()

  $('a.image-popup').magnificPopup
    type: 'image'


  $('.colorpicker').spectrum
    color: 'red'
    showPaletteOnly: true
    showPalette:true
    palette: [
      ['8318FA', '#B6B612', '#A6B255', '#B58C26', '#3F410F', '#D92E22', '#9D8539', '#7D0306', '#8BB18D', '#7CDBDD', '#AB7DA9', '#F3678A', '#107238', '#8B34CB', '#238FF5', '#DA048A', '#E7E4F7', '#E1763A', '#EB6202', '#E26616', '#CE1C39', '#710660', '#E515B4', '#0A06C0', '#9B3014',
      '7D38A7', '#12829E', '#2743C0', '#8DA847', '#9D3CE4', '#1524DC', '#BFF428', '#35F78C', '#71929E', '#A3BDFA', '#20F45D', '#11D05F', '#89580D', '#48FEB9', '#C6C84C', '#26C2A2', '#0B3EC8', '#E423C2', '#11B0F7', '#9CD641', '#278F92', '#1AE48A', '#D6FF9C', '#2ADEBD', '#54A3BF']
    ]