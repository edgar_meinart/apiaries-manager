module JournalsHelper

  def information_table_data(post)
    html = ''
    data_attributes = %w(family_strength wax brood honey family_weight)
    data_attributes.each do |attribute|
      attribute = post.try(attribute)
      value = attribute.present? ? attribute : 'Без изменений'
      html << content_tag( 'td', value)
    end    
    html.html_safe
  end

end
