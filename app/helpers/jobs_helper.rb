module JobsHelper
  def job_status(time)
    if time.today?
      return 'warning'
    elsif Date.today > time
      return 'error'
    elsif Date.today < time
      return 'success'
    end
    time.today?
  end

end
