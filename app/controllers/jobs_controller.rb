class JobsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_apiary!, except: [ :index, :destroy, :edit, :update ]
  before_action :load_family!, except: [ :index, :destroy, :edit, :update ]
  before_action :laod_job!, except: [ :index, :new, :create ]

  def new
    @job = Job.new
  end


  def create
    @job = @family.jobs.build job_params 
    if @job.save
      redirect_to @apiary
    else
      render action: :new
    end
  end


  def index
    families = Family.joins(:jobs).where( apiary_id: current_user.apiary_ids ).pluck :id
    @jobs = Job.where(family_id: families).order('date ASC')
  end

  def edit
    
  end

  def update
    if @job.update(job_params)
      redirect_to jobs_path
    else
      render action: :edit
    end
  end


  def destroy
    @job.destroy
    redirect_to jobs_path
  end

  private

    def load_apiary!
      @apiary = current_user.apiaries.find params[:apiary_id]
    end

    def load_family!
      @family = @apiary.families.find params[:family_id]
    end

    def laod_job!
      @job = Job.find params[:id]
    end

    def job_params
      params.require(:job).permit :date, :description
    end
end
  