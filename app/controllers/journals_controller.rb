class JournalsController < ApplicationController
  before_action :authenticate_user!
  before_action :get_apiary!
  before_action :get_family!
  before_action :get_post!, except: [ :new, :create ]

  def new
    @post = @family.posts.build
    @post.attachments.build
  end


  def create
    @post = @family.posts.build post_params
    if @post.add_attachment_from_and_save( params[:post][:temporary_attachments] ) 
      redirect_to @apiary 
    else
      render action: :new
    end
  end


  def show
    
  end


  def edit
    
  end


  def update
    
  end


  def destroy
    
  end




  private

    def get_apiary!
      @apiary = current_user.apiaries.find params[:apiary_id]
    end

    def get_family!
      @family = @apiary.families.find params[:family_id]
    end

    def get_post!
      @post = @family.posts.find params[:id]
    end

    def post_params 
      params.require(:post).permit :text, :year, :wax, :brood, :honey, :family_weight, :family_strength, body: [ :frame_count, :size_width, :size_height, :color ], new_body: [ :frame_count, :size_width, :size_height, :color ]  
    end


end
