class CardsController < ApplicationController
  before_action :authenticate_user!
  before_action :get_apiary!
  before_action :get_family!

  def new
    @family_characteristic = @family.family_characteristics.build
    @womb = @family.wombs.build @family.wombs.first.attributes
  end

  def create
    @family_characteristic = @family.family_characteristics.build card_params[:family_characteristic]
    @womb = @family.wombs.build card_params[:womb]

    if @family_characteristic.save && @womb.save
      redirect_to @apiary
    else
      render action: :new
    end
  end


  def show
  end

  private

    def get_apiary!
      @apiary = current_user.apiaries.find params[:apiary_id]
    end

    def get_family!
      @family = @apiary.families.find params[:family_id]
    end

    def card_params 
      params.require(:family).permit womb: [ :year, :breed, :prolificacy, :label, :comment ], family_characteristic: [ :honey_productivity, :wax_productivity, :family_strength, :comment, :date ]
    end


end
