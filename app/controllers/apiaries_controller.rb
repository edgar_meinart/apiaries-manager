class ApiariesController < ApplicationController
  before_action :authenticate_user!
  before_action :load_apiary!, only: [ :edit, :update, :show, :destroy ]


  def index
    @apiaries = current_user.apiaries order: "created_at DESC"
  end

  def new
    @apiary = Apiary.new
  end

  def create
    @apiary = Apiary.new apiary_params
    if @apiary.save
      current_user.apiaries << @apiary
      redirect_to apiary_path(@apiary)
    else
      render action: "new"
    end
  end

  def edit
  end

  def update
    if @apiary.update apiary_params
      redirect_to apiaries_path
    else
      render action: 'edit'
    end
  end

  def show
  end

  def destroy
    if @apiary.destroy
      redirect_to apiaries_path
    end
  end


  private

    def load_apiary!
      @apiary = Apiary.find( params[:id] )
    end

    def apiary_params
      # params.require(:apiary).permit :num, bodies_attributes: [ :frame_count, :size_width, :size_height, :color ], wombs_attributes: [ :year, :breed, :prolificacy ]
      params.require(:apiary).permit :title, :id
    end
end
