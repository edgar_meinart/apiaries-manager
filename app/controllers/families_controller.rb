class FamiliesController < ApplicationController
  before_action :authenticate_user!
  before_action :load_apiary!#, except: [ :new ]
  before_action :load_family!, only: [ :edit, :update, :show, :destroy, :dublicate ]


  def index
  end

  def new
    @family = Family.new
    @family.bodies.build
    @family.wombs.build
  end

  def create
    @family = @apiary.families.build family_params
    if @family.save
      flash[:notice] = 'Семья успешно сохранена'
      redirect_to @apiary
    else
      render action: "new"
    end
  end

  def dublicate
    if @family.dublicate
      redirect_to @apiary
    else
      render text: "dfdsf"
    end
  end

  def edit
  end

  def update
    if @family.update family_params
      @family.bodies.first.update family_params[:body]
      flash[:notice] = 'Обнавление для семьи сохранено'
      redirect_to @apiary
    else
      render action: 'edit'
    end
  end

  def show
  end

  def destroy
    if @family.destroy
      flash[:notice] = 'Семья успешна удалена'
      redirect_to @apiary
    end
  end


  private

    def load_apiary!
      @apiary = Apiary.find( params['apiary_id'] )
    end

    def load_family!
      @family = @apiary.families.find(params[:id])
    end

    def family_params
      # params.require(:apiary).permit :num, bodies_attributes: [ :frame_count, :size_width, :size_height, :color ], wombs_attributes: [ :year, :breed, :prolificacy ]
      params.require(:family).permit :num, :id , bodies_attributes: [ :frame_count, :size_width, :size_height, :color ], body: [ :id, :frame_count, :size_width, :size_height, :color ], wombs_attributes: [ :year, :breed, :prolificacy ]
    end
end
