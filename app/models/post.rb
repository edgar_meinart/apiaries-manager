class Post < ActiveRecord::Base
  attr_accessor :temporary_attachments, :body, :new_body

  has_many :attachments, dependent: :destroy
  has_one :body_revision, dependent: :destroy, class_name: 'Body'
  has_one :root_body_revision, dependent: :destroy, as: :post_for_root_body, class_name: 'Body'

  belongs_to :family

  accepts_nested_attributes_for :attachments
  accepts_nested_attributes_for :family

  after_save :after_save_show_body, unless: -> { body.blank? }
  after_create :save_new_body, unless: -> { new_body.blank? }

  def after_save_show_body
    body_wrapper = self.family.bodies.first
    body_wrapper.assign_attributes body
    body_wrapper.save_and_create_revision_for! family_id, id, true
  end

  def save_new_body
    body_wrapper = family.bodies.build new_body
    body_wrapper.save_and_create_revision_for! family_id, id, false
  end

  def add_attachment_from_and_save( array = [] )
    unless array.blank?
      array.each do |file|
        self.attachments << Attachment.create( asset: file )
      end
      self.save
    else
      self.save
    end
  end

end
