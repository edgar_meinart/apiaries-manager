class ProfileBody < ActiveRecord::Base
  belongs_to :body
  belongs_to :root_body, :class_name => 'Body'
end
