class Family < ActiveRecord::Base
  attr_accessor :body

  has_many :wombs, dependent: :destroy
  has_many :family_characteristics, dependent: :destroy
  has_many :bodies, dependent: :destroy
  has_many :jobs, dependent: :destroy
  has_many :body_histories, dependent: :destroy

  has_many :posts, dependent: :destroy


  validates :num, presence: true

  accepts_nested_attributes_for :wombs
  accepts_nested_attributes_for :bodies
  accepts_nested_attributes_for :family_characteristics


  def dublicate
    new_family = self.dup
    new_family.num = "copy of #{new_family.num}"
    new_family.womb = self.womb.dup
    new_family.bodies << self.bodies.first.dup

    return new_family  if new_family.womb.save && new_family.bodies.first.save && new_family.save
    nil
  end

end
 
