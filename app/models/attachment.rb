class Attachment < ActiveRecord::Base
  belongs_to :post

  has_attached_file :asset,
    styles: { medium: "300x300>", thumb: "100x100>" },
    path: ':rails_root/public/uploads/attacments/:id.:style.png',
    url: '/uploads/attacments/:id.:style.png',
    default_url: "/images/:style/missing.png"
    
  validates_attachment_content_type :asset, :content_type => /\Aimage\/.*\Z/
end
