class Apiary < ActiveRecord::Base
  has_many :families, dependent: :destroy
  belongs_to :user

  validates :title, presence: true
end
