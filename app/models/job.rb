class Job < ActiveRecord::Base
  belongs_to :family


  validates :date, :description, presence: true
end
