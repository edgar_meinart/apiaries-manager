class Body < ActiveRecord::Base
  attr_accessor :update_post_id

  has_one :profile_body, dependent: :destroy
  has_one :root_body, :through => :profile_body, dependent: :destroy

  belongs_to :family
  belongs_to :post
  belongs_to :post_for_root_body

  validates :frame_count, :size_width, :size_height, :color, presence: true

  after_create :update_history, unless: -> { revision? }
  after_create :update_history, unless: -> { revision? }


  #DEBUG: How it works? It's a magic!
  def save_and_create_revision_for!( family_id, post_id, root, revision = false )
    unless revision
      self.assign_attributes family_id: family_id
      save_and_create_revision_for!( family_id, post_id, root, true ) if self.save  
    else
      revision_body = Body.new self.attributes
      revision_body.revision = true
      if root 
        revision_body.id = nil
        revision_body.family_id = nil
        Post.find(post_id).root_body_revision = revision_body if revision_body.save
      else
        revision_body.assign_attributes post_id: post_id, id: nil, family_id: nil
        revision_body.save
      end
      revision_body.root_body = self
    end
  end
  
  private

    def update_history
      p "=--------------- POSTID: #{self.post} ------------------="
      if self.family.present? 
        history = self.family.body_histories.build( body_count: self.family.bodies.count, post_id: self.post_id ) 
        history.save
      end
    end

end
