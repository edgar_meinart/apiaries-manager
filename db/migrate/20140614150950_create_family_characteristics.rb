class CreateFamilyCharacteristics < ActiveRecord::Migration
  def change
    create_table :family_characteristics do |t|
      t.datetime :date
      t.integer :family_id
      t.float :honey_productivity
      t.float :wax_productivity
      t.float :family_strength
      t.text :comment

      t.timestamps
    end
  end
end
