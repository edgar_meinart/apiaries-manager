class AddIdToBody < ActiveRecord::Migration
  def change
    add_column :bodies, :post_for_root_body_id, :integer 
    add_column :bodies, :post_for_root_body_type, :string 
  end
end
