class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :family_id

      t.timestamps
    end
  end
end
