class CreateFamilies < ActiveRecord::Migration
  def change
    create_table :families do |t|
      t.string :num
      t.integer :apiary_id

      t.timestamps
    end
  end
end
