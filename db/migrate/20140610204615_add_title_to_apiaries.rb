class AddTitleToApiaries < ActiveRecord::Migration
  def change
    add_column :apiaries, :title, :string
  end
end
