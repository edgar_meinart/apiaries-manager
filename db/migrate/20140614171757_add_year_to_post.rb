class AddYearToPost < ActiveRecord::Migration
  def change
    add_column :posts, :year, :datetime
  end
end
