class AddParamsToWombs < ActiveRecord::Migration
  def change
    add_column :wombs, :label, :string
    add_column :wombs, :comment, :text
  end
end
