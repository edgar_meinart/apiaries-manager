class AddInformationParametersToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :family_strength, :integer
    add_column :posts, :brood, :float
    add_column :posts, :wax, :float 
    add_column :posts, :honey, :float
    add_column :posts, :family_weight, :float
  end
end
