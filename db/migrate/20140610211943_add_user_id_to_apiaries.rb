class AddUserIdToApiaries < ActiveRecord::Migration
  def change
    add_column :apiaries, :user_id, :integer
  end
end
