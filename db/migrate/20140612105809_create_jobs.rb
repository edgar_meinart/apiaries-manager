class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.datetime :date
      t.text :description
      t.integer :family_id

      t.timestamps
    end
  end
end
