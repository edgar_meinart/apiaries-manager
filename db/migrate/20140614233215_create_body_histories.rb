class CreateBodyHistories < ActiveRecord::Migration
  def change
    create_table :body_histories do |t|
      t.integer :body_count, default: 0
      t.integer :family_id
      t.integer :post_id
      t.timestamps
    end
  end
end
