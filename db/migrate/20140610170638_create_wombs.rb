class CreateWombs < ActiveRecord::Migration
  def change
    create_table :wombs do |t|
      t.datetime :year
      t.string :breed
      t.string :prolificacy
      t.integer :family_id

      t.timestamps
    end
  end
end
