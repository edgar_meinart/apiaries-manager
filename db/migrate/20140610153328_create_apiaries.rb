class CreateApiaries < ActiveRecord::Migration
  def change
    create_table :apiaries do |t|
      t.string :num

      t.timestamps
    end
  end
end
