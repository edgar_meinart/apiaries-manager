class CreateBodies < ActiveRecord::Migration
  def change
    create_table :bodies do |t|
      t.integer :frame_count
      t.integer :size_width
      t.integer :size_height
      t.string :color
      t.integer :family_id

      t.timestamps
    end
  end
end
