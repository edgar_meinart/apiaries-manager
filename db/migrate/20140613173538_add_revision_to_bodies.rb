class AddRevisionToBodies < ActiveRecord::Migration
  def change
    add_column :bodies, :revision, :boolean, default: false
  end
end
