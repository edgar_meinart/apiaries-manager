class CreateProfileBodies < ActiveRecord::Migration
  def change
    create_table :profile_bodies do |t|
      t.integer :body_id
      t.integer :root_body_id

      t.timestamps
    end
  end
end
