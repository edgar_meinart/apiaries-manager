# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140614233215) do

  create_table "apiaries", force: true do |t|
    t.string   "num"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
    t.integer  "user_id"
  end

  create_table "attachments", force: true do |t|
    t.integer  "post_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "asset_file_name"
    t.string   "asset_content_type"
    t.integer  "asset_file_size"
    t.datetime "asset_updated_at"
  end

  create_table "bodies", force: true do |t|
    t.integer  "frame_count"
    t.integer  "size_width"
    t.integer  "size_height"
    t.string   "color"
    t.integer  "family_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "post_id"
    t.boolean  "revision",                default: false
    t.integer  "post_for_root_body_id"
    t.string   "post_for_root_body_type"
  end

  create_table "body_histories", force: true do |t|
    t.integer  "body_count", default: 0
    t.integer  "family_id"
    t.integer  "post_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "families", force: true do |t|
    t.string   "num"
    t.integer  "apiary_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "family_characteristics", force: true do |t|
    t.datetime "date"
    t.integer  "family_id"
    t.float    "honey_productivity"
    t.float    "wax_productivity"
    t.float    "family_strength"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "jobs", force: true do |t|
    t.datetime "date"
    t.text     "description"
    t.integer  "family_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "posts", force: true do |t|
    t.integer  "family_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "text"
    t.integer  "family_strength"
    t.float    "brood"
    t.float    "wax"
    t.float    "honey"
    t.float    "family_weight"
    t.datetime "year"
  end

  create_table "profile_bodies", force: true do |t|
    t.integer  "body_id"
    t.integer  "root_body_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "wombs", force: true do |t|
    t.datetime "year"
    t.string   "breed"
    t.string   "prolificacy"
    t.integer  "family_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "label"
    t.text     "comment"
  end

end
